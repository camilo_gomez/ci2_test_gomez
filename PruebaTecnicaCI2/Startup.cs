﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PruebaTecnicaCI2.Startup))]
namespace PruebaTecnicaCI2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
